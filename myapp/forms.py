from django import forms

class GenericForm(forms.Form):
    file_upload = forms.FileField()

    def clean_file_upload(self):
        return self.cleaned_data['file_upload']


from django.shortcuts import render, redirect
from .forms import GenericForm
import pandas as pd
import statsmodels.formula.api as sm
from .utils import *
from django.core.files.storage import FileSystemStorage
# Create your views here.
import rpy2.robjects as robjects


def home(request):
    if request.method=="GET":
        f = GenericForm()
        return render(request, 'myapp/index.html', {'form':f,})
    elif (request.method == "POST"):
        print("INSIDE POST")
        print(request.POST.get('file_upload'))

        if request.POST.get('file_upload') is None:
            print("YES IT IS PRESENT")
        
            if request.FILES['file_upload'] is not None:
                # Take the File Name and get it's Extension.
                file_name_parameter = request.FILES['file_upload']
                file_extension = test_file_format(file_name_parameter.name)
                
                # If the extension is other than Xlsx, Csv. Raise error.
                if file_extension == "csv" or file_extension == "xlsx":
                    # print(request.FILES['file_upload'])
                    print("INITIAL POST")
                    # home_page_post_data_file = request.FILES['file_upload']
                    file = request.FILES['file_upload']
                    print("FILE NAME IS ", file)

                    data = pd.read_csv(file, index_col = False)
                    # column_to_ignore = " "
                    # data_frame = data.drop(column_to_ignore, axis=1)
                    file_name = file.name

                    print("DATA IS HERE --->")
                    print(data.columns)

                    data_html = data.to_html()
                    columns = list(data.columns)
                    items = columns
                    from .utils import accept_columns
                    accept_columns(columns)

                    fs = FileSystemStorage()
                    filename_save = fs.save(file_name, file)

                    context = {'loaded_data': data_html, 'columns': columns, 'items':items, 'file_name': file_name}
                    return render(request, 'myapp/index.html', context)
                else:
                    file_format_error_message = "Please upload either .CSV or .xlsx file formats "
                    context = {'file_format_error_message': file_format_error_message}

                    return render(request, 'myapp/index.html', context)
        
        else:
            return redirect('home')

def predict(request):
    if request.method == "POST" and request.POST.get('selectedTask') is not None:
        predict_value = request.POST.get('selectedTask')
        print(predict_value)
        checks = request.POST.getlist('checks')
        print(checks)
        if predict_value and checks:
            from .utils import create_var
            formula = ""

            # Split Values are the Checked Checkboxes (Independent Variables)
            split_values = create_var(checks)
            if split_values:
                for split_value in split_values:
                    if split_value == predict_value:
                        continue
                    else:
                        formula = formula+split_value+str("+")
                
                formula = formula[:-1]
                print(formula)
                print(predict_value)

                # df_values = [predict_value, ]
                # for split_value in split_values:
                #     df_values.append(split_value)

                # print(df_values)

                # print("DF IS ---------> \n",df)

                # Get the File name from the Hidden Input Element
                file_name = request.POST.get('filename')
                print("TYPE of File name is : ", type(file_name))
                print("FILE NAME IS ", file_name)
                # Read the CSV File which is saved in the "MEDIA" Directory in Server
                raw_data = pd.read_csv('media/'+file_name)
                columns = list(raw_data.columns)

                print("COLUMNS ARE HERE -------> ", columns)
                # Assigning the Values of Each Column into a Dictionary
                key_val_pairs = {}
                for col in columns:
                    key_val_pairs[col] = list(raw_data[col])
                print(key_val_pairs)

                # Pass the Dictionary of Column values of csv to DataFrame
                df = pd.DataFrame(key_val_pairs)
                # print(df.columns)

                # Create New Dictionary to have Data of Columns
                new_dict = {}
                new_dict[predict_value] = raw_data[predict_value]
                
                # Split Values are the Checked Checkboxes (Independent Variables)
                for split_value in split_values:
                    new_dict[split_value] = raw_data[split_value]
                print(new_dict)

                # To pass Dynamic Keys to get the Values of Columns
                dynamic_keys = ""
                for split_value in split_values:
                    if split_value == predict_value:
                        print("SKIP SKIP")
                        continue
                    else:
                        dynamic_keys = dynamic_keys+"new_dict['"+split_value+"'] "

                dynamic_keys_split = dynamic_keys.split(" ")
                dynamic_keys_split_2 = ""
                for val in dynamic_keys_split:
                    dynamic_keys_split_2 = dynamic_keys_split_2+str(val)+"+"

                formula_last = dynamic_keys_split_2[0:-2]

                result = sm.ols(formula="new_dict[predict_value]~"+formula_last, data=raw_data).fit()
                prediction_vals = result.predict(df)

                # raw_data['Prediction'] = pd.Series(prediction_vals)

                prediction_vals.to_csv('Results/predict_values.csv')
                
                data = pd.read_csv('Results/predict_values.csv', index_col=False)
                data_html = data.to_html()

        context = {'predict_value':predict_value, 'loaded_data': data_html}
        return render(request, 'myapp/predict.html', context)
    elif request.method == "GET":
        return redirect('home')
from django.db import models

# Create your models here.
class FileUpload(models.Model):
    file_upload = models.FileField(upload_to='files/')
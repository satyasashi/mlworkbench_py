def accept_columns(columns):
    return columns


def create_var(checks):
    result = []

    for checkbox in checks:
        result.append(checkbox)
    return result

def test_file_format(file_name):
    full_file_name = file_name
    split_file_name = full_file_name.split(".")
    if len(split_file_name) > 1:
        extension = split_file_name[1]
        return extension
    else:
        extension = None
        return extension